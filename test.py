import requests
url = 'http://127.0.0.1:5000/'

data = {
    'duedate': 'September 1, 2020',
    'from_addr': {
        'addr1': 'Alexandria, Egypt',
        'addr2': 'Somuha 30002',
        'company_name': 'Python Arabia'
    },
    'invoice_number': 156,
    'items': [{
            'charge': 500.0,
            'title': 'App Development'
        },
        {
            'charge': 85.0,
            'title': 'Hosting (6 months)'
        },
        {
            'charge': 10.0,
            'title': 'Domain name (1 year)'
        }
    ],
    'to_addr': {
        'company_name': 'Forweb.io',
        'person_email': 'john@example.com',
        'person_name': 'Fahd Mannaa'
    }
}
html = requests.post(url, json=data)
with open('invoice.pdf', 'wb') as f:
    f.write(html.content)